import os
import unittest
import pandas

from census import census_load, census_prep, census_ana

os.environ["All_PATH"] = 'adult.data'


class MyTestCase(unittest.TestCase):
    def test_census_load(self):
        loaded_data = census_load()
        self.assertEqual(type(loaded_data), pandas.DataFrame)
        self.assertGreater(len(loaded_data), 0)

    def test_census_prep(self):
        new_data = census_load()
        new_data1 = new_data[:100]
        actual = census_prep(new_data)
        self.assertTrue(len(new_data1), len(actual))

    def test_census_ana(self):
        loaded_data = census_load()
        expected = census_ana(loaded_data)
        self.assertTrue(38, expected)
        self.assertTrue(56, expected)


if __name__ == '__main__':
    unittest.main()
