import census


def run():
    df = census.census_load()
    census.census_prep(df)
    census.census_rel(df)
    census.census_ana(df)


if __name__ == '__main__':
    run()
