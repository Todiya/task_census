import os
import pandas as pd


# loading tha dataframe ,adding col name
def census_load():
    if os.getenv('ALL_PATH'):
        with open(os.getenv('ALL_PATH'), "r") as my_file:
            columns = ['age', 'type_employer', 'fnlwgt', 'education', 'education_num', 'marital', 'occupation',
                       'relationship', 'race', 'sex', 'captital_gain', 'capital_loss', 'hr_per_week', 'country',
                       'income']
            df = pd.read_csv(my_file, delimiter=', ', names=columns, engine='python')
            print(df.head(5))
            counts = df.groupby('country').size()
            print(counts.head())
        return df


# prepare subset of 100 rows
def census_prep(df):
    random_subset = df.sample(n=100)
    print(random_subset, '\n', '\n')
    return random_subset, df


# analysis of income and other col=sex
def census_rel(df):
    ml = df[(df.sex == "Male")]
    ml1 = df[(df.sex == "Male") & (df.income == ">50K")]
    fm = df[(df.sex == 'Female')]
    fm1 = df[(df.sex == 'Female') & (df.income == '>50K')]

    df1 = df[(df.income == '>50K')]
    print('The rate of people with high income is:', float(len(df1) / float(len(df)) * 100), '%.')
    print('The rate of men with high income is:', float(len(ml1) / float(len(ml)) * 100), '%.')
    print('The rate of women with high income is :', float(len(fm1) / float(len(fm)) * 100), '%.', '\n')

    tab = pd.crosstab(df.income, df.sex, margins=True)
    print('The contingency table for Income vs Sex')
    print(tab, '\n', '\n')


# maximum occurance of age where income is '>50'
def census_ana(df):
    grp1 = df.reset_index().groupby(['age', 'income']).size()
    grp1 = pd.Series.to_frame(grp1)
    grp1.columns = ['many']
    grp1.reset_index(level=[0, 1], inplace=True)
    max_occurance = grp1.loc[grp1['income'] == '>50K']
    result = max_occurance[max_occurance['many'] == max_occurance['many'].max()]
    print(result, '\n')
